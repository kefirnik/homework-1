
#ifndef MAIN
#define MAIN

const int MATRIX_SIZE = 12;

void printFirstNegativeInArea(int [][MATRIX_SIZE]);

void printMatrix(int [][MATRIX_SIZE]);

void printTaskBAnswer(int [][MATRIX_SIZE]);

void printMinLineFromMax(int [][MATRIX_SIZE]);

void sumOfMinElementsColumns(int matrix[][MATRIX_SIZE]);

void swapMatrixBlocks(int [][MATRIX_SIZE]);

void addMatrixToMatrix(int matrix[][MATRIX_SIZE]);

void determinant(int matrix[][MATRIX_SIZE]);

#endif //MAIN
