#include <iostream>
#include <ctime>
#include <iomanip>
#include "main.h"
using namespace std;

#define CLEAR_SCREEN (system("cls"))
#define PAUSE (system("pause"))
#define NEW_LINE (cout << endl)

int main()
{
    srand(time(0));
    int matrix[MATRIX_SIZE][MATRIX_SIZE]{};

    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            matrix[i][j] = rand() % 12 + 1;
        }
    }

    int option = true;
    while (option)
    {
        CLEAR_SCREEN;
        cout <<
             "0. Exit\n"
             "1. Print matrix\n"
             "2. First negative in colored area\n"
             "3. Sum of absolutes of elements of first negative in line\n"
             "4. Minimum number from maximums in lines\n"
             "5. Sum of minimum elements in columns\n"
             "6. Swap matrix blocks\n"
             "7. Finding the determinant of a matrix \n"
             "8. Additional matrix creation and addition \n";
        cout << "Enter the number of the task you want to see: ";
        cin >> option;

        switch (option)
        {
            case 0:
                CLEAR_SCREEN;
                break;
            case 1:
                CLEAR_SCREEN;
                printMatrix(matrix);
                break;
            case 2:
                CLEAR_SCREEN;
                printFirstNegativeInArea(matrix);
                break;
            case 3:
                CLEAR_SCREEN;
                printTaskBAnswer(matrix);
                break;
            case 4:
                CLEAR_SCREEN;
                printMinLineFromMax(matrix);
                break;
            case 5:
                CLEAR_SCREEN;
                sumOfMinElementsColumns(matrix);
                break;
            case 6:
                CLEAR_SCREEN;
                swapMatrixBlocks(matrix);
                break;
            case 7:
                CLEAR_SCREEN;
                determinant(matrix);
                break;
            case 8:
                CLEAR_SCREEN;
                addMatrixToMatrix(matrix);
                break;
            default:
                cout << "Error! Wrong input!" << endl;
        }
        PAUSE;
    }
}

void printFirstNegativeInArea(int matrix[][MATRIX_SIZE])
{
    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 4; j < MATRIX_SIZE; j++)
        {
            if (matrix[i][j] < 0)
            {
                cout << "The first negative number in the area line: " << i << ", column: " << j << endl;
                cout << "Value: " << matrix[i][j] << endl;
                return;
            }
        }
    }
}

void printMatrix(int matrix[][MATRIX_SIZE])
{
    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            cout << setw(4) << matrix[i][j];
        }
        NEW_LINE;
    }
}

void printTaskBAnswer(int matrix[][MATRIX_SIZE])
{
    int sum = 0;
    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        bool afterNegative = false;
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            if (afterNegative)
                sum += abs(matrix[i][j]);
            else
                afterNegative = matrix[i][j] < 0;
        }
    }
    cout << sum << endl;
}

void printMinLineFromMax(int matrix[][MATRIX_SIZE])
{
    int maximums[MATRIX_SIZE]{};
    int minimumNumLineFromMax{};

    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            if (matrix[i][j] > maximums[i])
            {
                maximums[i] = matrix[i][j];
            }
        }
    }

    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        if (maximums[i] < minimumNumLineFromMax)
        {
            minimumNumLineFromMax = i;
        }
    }

    cout << "Minimum number's row from maximum numbers in each row: " << minimumNumLineFromMax << endl;
}

void sumOfMinElementsColumns(int matrix[][MATRIX_SIZE])
{
    int resultSum{};

    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        int minimumElement = 13;
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            if (matrix[j][i] < minimumElement)
            {
                minimumElement = matrix[j][i];
            }
        }
        resultSum+= minimumElement;
    }

    cout << "Sum of minimum element in every column: " << resultSum << endl;
}

void swapMatrixBlocks(int matrix[][MATRIX_SIZE])
{
    if (MATRIX_SIZE % 3 != 0)
    {
        cout << "ERROR: array size is not a multiple of 3" << endl;
        return;
    }

    for (int i = 0; i < MATRIX_SIZE / 3; i++)
    {
        for (int j = 0; j < MATRIX_SIZE / 3; j++)
        {
            int blockStart1 = 2 * (MATRIX_SIZE / 3);

            matrix[i][j] += matrix[blockStart1 + i][blockStart1 + j];
            matrix[blockStart1 + i][blockStart1 + j] = matrix[i][j] - matrix[blockStart1 + i][blockStart1 + j];
            matrix[i][j] -= matrix[blockStart1 + i][blockStart1 + j];

            int blockStart2 = MATRIX_SIZE / 3;

            matrix[i][blockStart1 + i] += matrix[blockStart2 + i][blockStart2 + j];
            matrix[blockStart2 + i][blockStart2 + j] = matrix[i][blockStart1 + i] - matrix[blockStart2 +
                                                                                           i][blockStart2 + j];
            matrix[i][blockStart1 + i] -= matrix[blockStart2 + i][blockStart2 + j];
        }
    }
}

void determinant(int matrix[][MATRIX_SIZE])
{
    long long determinant = 0;

    for (int i = 0; i < MATRIX_SIZE; ++i)
    {
        long long temp = 1;
        for (int j = 0; j < MATRIX_SIZE; ++j)
        {
            int posY = (i + j) % MATRIX_SIZE;
            temp *= matrix[posY][j];
        }
        determinant += temp;
    }

    for (int i = 0; i < MATRIX_SIZE; ++i)
    {
        long long temp = 1;
        for (int j = MATRIX_SIZE-1; j >= 0; --j)
        {
            int posY = (i + MATRIX_SIZE - j) % MATRIX_SIZE;
            temp *= matrix[posY][j];
        }
        determinant -= temp;
    }

    cout << determinant << endl;
}

void addMatrixToMatrix(int matrix[][MATRIX_SIZE])
{
    int matrix2[MATRIX_SIZE][MATRIX_SIZE]{};

    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            matrix2[i][j] = rand() % 12 + 1;
        }
    }

    for (int i = 0; i < MATRIX_SIZE; ++i)
    {
        for (int j = 0; j < MATRIX_SIZE; ++j)
        {
            matrix[i][j] += matrix2[i][j];
        }
    }
}